//: Playground - noun: a place where people can play

import UIKit

// Declare two integers in the same line
var firstInteger = 0, secondInteger = 255

// Declare decimal numbers
var discount = 0.3

// Type annotation: explicitly telling program variable type
var secondDiscount:Double = 0.5
var intWhichIsDouble:Double = 5

/*
This is multi-line comment
paragraph 1
paragraph 2
paragraph 3
*/

// Declare two integers 
var first:Double = 49
var second = 10

// Division will be an integer two
var division = first/second
var downPayment = 10.5

var priceAfterDiscount = first * (1.0 - discount);
priceAfterDiscount = priceAfterDiscount - downPayment

// TypeAlias example

typealias WholeNumber = Int
typealias NumberWithDecimal = Double

var realNumber:NumberWithDecimal = 5.0

// Use of Numeric literals 
var largeNumber = 1_000_000_000_000



